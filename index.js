// alert('hi');

// A document is your webpage
console.log(document);

// Targeting the first name input field

// document.querySelector('#txt-first-name');

// alternatively:

// document.getElementById('txt-first-name');

// for other types
	// document.getElementByClassName('class-name');
	// document.getElementByTagName('tag-name')

// Contain the query selector code in a constant

// const txtFirstName = document.querySelector("#txt-first-name");

// const spanFullName = document.querySelector("#span-full-name");

// Event Listeners

// txtFirstName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtFirstName.value;
// });

// Multiple Listeners
	// e is a shorthand for event

// txtFirstName.addEventListener('keyup', (e) => {
// 	console.log(e.target);
// 	console.log(e.target.value);
// });

// A C T I V I T Y S O L U T I O N

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

const updateFullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`
};

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);
